-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 26, 2022 at 01:41 AM
-- Server version: 5.7.31
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `category`, `created_date`, `updated_date`, `status`) VALUES
(4, 'CODEIGNITER', 'Welcome to CodeIgniter\r\nCodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.\r\n\r\nWho is CodeIgniter For?\r\nCodeIgniter is right for you if:\r\n\r\nYou want a framework with a small footprint.\r\nYou need exceptional performance.\r\nYou need broad compatibility with standard hosting accounts that run a variety of PHP versions and configurations.\r\nYou want a framework that requires nearly zero configuration.\r\nYou want a framework that does not require you to use the command line.\r\nYou want a framework that does not require you to adhere to restrictive coding rules.\r\nYou are not interested in large-scale monolithic libraries like PEAR.\r\nYou do not want to be forced to learn a templating language (although a template parser is optionally available if you desire one).\r\nYou eschew complexity, favoring simple solutions.\r\nYou need clear, thorough documentation.  ', 'Programming', '2022-10-23 11:29:39', '0000-00-00 00:00:00', 'Draft'),
(5, 'pogfxhghj', 'Welcome to CodeIgniter\r\nCodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.\r\n\r\nWho is CodeIgniter For?\r\nCodeIgniter is right for you if:\r\n\r\nYou want a framework with a small footprint.\r\nYou need exceptional performance.\r\nYou need broad compatibility with standard hosting accounts that run a variety of PHP versions and configurations.\r\nYou want a framework that requires nearly zero configuration.\r\nYou want a framework that does not require you to use the command line.\r\nYou want a framework that does not require you to adhere to restrictive coding rules.\r\nYou are not interested in large-scale monolithic libraries like PEAR.\r\nYou do not want to be forced to learn a templating language (although a template parser is optionally available if you desire one).\r\nYou eschew complexity, favoring simple solutions.\r\nYou need clear, thorough documentation.\r\n', 'NEWS', '2022-10-24 11:29:45', '0000-00-00 00:00:00', 'Publish'),
(6, 'pgyuyffydt', 'Welcome to CodeIgniter\r\nCodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.\r\n\r\nWho is CodeIgniter For?\r\nCodeIgniter is right for you if:\r\n\r\nYou want a framework with a small footprint.\r\nYou need exceptional performance.\r\nYou need broad compatibility with standard hosting accounts that run a variety of PHP versions and configurations.\r\nYou want a framework that requires nearly zero configuration.\r\nYou want a framework that does not require you to use the command line.\r\nYou want a framework that does not require you to adhere to restrictive coding rules.\r\nYou are not interested in large-scale monolithic libraries like PEAR.\r\nYou do not want to be forced to learn a templating language (although a template parser is optionally available if you desire one).\r\nYou eschew complexity, favoring simple solutions.\r\nYou need clear, thorough documentation.', 'NEWS', '2022-10-23 11:29:49', '0000-00-00 00:00:00', 'Publish'),
(7, 'jggjgku', 'Welcome to CodeIgniter\r\nCodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.\r\n\r\nWho is CodeIgniter For?\r\nCodeIgniter is right for you if:\r\n\r\nYou want a framework with a small footprint.\r\nYou need exceptional performance.\r\nYou need broad compatibility with standard hosting accounts that run a variety of PHP versions and configurations.\r\nYou want a framework that requires nearly zero configuration.\r\nYou want a framework that does not require you to use the command line.\r\nYou want a framework that does not require you to adhere to restrictive coding rules.\r\nYou are not interested in large-scale monolithic libraries like PEAR.\r\nYou do not want to be forced to learn a templating language (although a template parser is optionally available if you desire one).\r\nYou eschew complexity, favoring simple solutions.\r\nYou need clear, thorough documentation.', 'NEWS', '2022-10-24 11:29:52', '0000-00-00 00:00:00', 'Publish'),
(8, 'POST 5', 'Welcome to CodeIgniter\r\nCodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.\r\n\r\nWho is CodeIgniter For?\r\nCodeIgniter is right for you if:\r\n\r\nYou want a framework with a small footprint.\r\nYou need exceptional performance.\r\nYou need broad compatibility with standard hosting accounts that run a variety of PHP versions and configurations.\r\nYou want a framework that requires nearly zero configuration.\r\nYou want a framework that does not require you to use the command line.\r\nYou want a framework that does not require you to adhere to restrictive coding rules.\r\nYou are not interested in large-scale monolithic libraries like PEAR.\r\nYou do not want to be forced to learn a templating language (although a template parser is optionally available if you desire one).\r\nYou eschew complexity, favoring simple solutions.\r\nYou need clear, thorough documentation.', 'NEWS', '2022-10-25 11:29:56', '0000-00-00 00:00:00', 'Publish');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
