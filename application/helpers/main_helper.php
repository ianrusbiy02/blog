<?php
defined('BASEPATH') or exit('No direct script access allowed');

function rupiahkan($nominal)
{
	return 'Rp ' . number_format($nominal, 0, ',', '.');
}

function chek_session()
{
	$ci = &get_instance();
	if ($ci->session->userdata('status_login') != 'oke') {
		redirect('user/login');
	}
}
