<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Post extends CI_Model{

	public function create($table,$data){
		$this->db->insert($table,$data);
	}

	public function read($table,$limit,$offset){
	
		$this->db->from($table);
		$this->db->where('status','Publish');
		$this->db->limit($limit,$offset);
		$this->db->order_by('id', 'DESC');
	
		$query = $this->db->get();
		if($query->num_rows() > 0){
			foreach($query->result_array() as $row){
				$data[] = $row;
			}

			$query->free_result();
		}
		else{
			$data = NULL;
		}
		
		return $data;
	}
	
	public function getcategory($table)
	{
		$q = $this->db->query("SELECT category FROM $table GROUP BY category ORDER BY id ");
		return $q->result();
	}
	
	

	public function edit($id,$table){
		$this->db->where('id',$id);
		$query = $this->db->get($table);
		if($query->num_rows() > 0){
			$data = $query->row();
			$query->free_result();
		}
		else{
			$data = NULL;
		}

		return $data;
	}

	public function update($id,$data,$table){
		$this->db->where('id',$id);
		$this->db->update($table,$data);		
	}
	

	public function delete($id,$table){
		$this->db->where('id',$id);
		$this->db->delete($table);
	}

	public function total_rows($table){
		return $this->db->count_all_results($table);
	}

	public function readArticle($id){
		$query = $this->db->query("SELECT * FROM posts WHERE id ='$id' ");
		return $query->result_array();
	}	

	
	
}