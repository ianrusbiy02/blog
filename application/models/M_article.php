<?php

class M_article extends CI_Model
{

	private $tablepost = 'posts';

	public function getAllArticle()
	{
		$query = $this->db->get($this->tablepost);
		return $query->result();
	}
	public function getAllPublishedArticle($status)
	{
		$this->db->where('status', $status);
		$query = $this->db->get($this->tablepost);
		return $query->result();
	}

	public function get_published($limit = null, $offset = null)
	{
		if (!$limit && $offset) {
			$query = $this->db
				->get_where($this->tablepost, ['draft' => 'FALSE']);
		} else {
			$query =  $this->db
				->get_where($this->tablepost, ['draft' => 'FALSE'], $limit, $offset);
		}
		return $query->result();
	}

	public function find_by_slug($slug)
	{
		if (!$slug) {
			return;
		}
		$query = $this->db->get_where($this->tablepost, ['slug' => $slug]);
		return $query->row();
	}

	public function find($id)
	{
		if (!$id) {
			return;
		}

		$query = $this->db->get_where($this->tablepost, array('id' => $id));
		return $query->row();
	}

	public function insertData($article)
	{
		return $this->db->insert($this->tablepost, $article);
	}

	public function updateArticle($article)
	{
		if (!isset($article['id'])) {
			return;
		}
		return $this->db->update($this->tablepost, $article, ['id' => $article['id']]);
	}

	public function deleteArticle($id)
	{
		if (!$id) {
			return;
		}
		$data = array( 
			'status'	=>  'Trash' , 
		);

		$this->db->where('id', $id);
		return $this->db->update($this->tablepost, $data);
	}

    public function count(){
        return $this->db->count_all($this->tablepost);
    }
}