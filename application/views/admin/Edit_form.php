<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https:///cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" >
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>

<body>
	<div>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-7">
		<a class="navbar-brand" href="#">Blog</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Posts
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="<?= site_url('posts/index') ?>">All Posts</a>
					<a class="dropdown-item" href="<?= site_url('posts/new') ?>">Add new</a>
					<a class="dropdown-item" href="<?= site_url('Home') ?>">Preview</a>
				</div>
			</li>
			</ul>
		</div>
		</nav>
	</div>
<div class="container">
	<div class="toolbar mb-3 mt-3"></div>
	<form action="" method="POST">
		<div class="form-group">
			<label for="title">Title*</label>
			<input class="form-control" type="text" name="title" placeholder="Please insert title" required title="Title required"  min="20" value="<?= $article->title ?>"/>
		</div>
		<div class="form-group">
				<label for="category">Category*</label>
				<input class="form-control" type="text" name="category" placeholder="Please insert category" required title="Category required" min="3" value="<?= $article->category ?>"/>
		</div>
		<div class="form-group">
			<label for="content">Content</label>
			<textarea class="form-control" id="content" name="content" rows="3" required title="Category required" min="200"><?= $article->content ?> </textarea>
		</div>
		<div class="form-group">
			<button type="submit" name="draft" class="btn btn-primary" value="true">Save to Draft</button>
			<button type="submit" name="publish" class="btn btn-success" value="false">Publish</button>
		</div>
	</form>
</body>

</html>