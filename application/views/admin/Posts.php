<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https:///cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>

<body>
	<div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-7">
  <a class="navbar-brand" href="#">Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Posts
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			<a class="dropdown-item" href="<?= site_url('posts/index') ?>">All Posts</a>
          	<a class="dropdown-item" href="<?= site_url('posts/new') ?>">Add new</a>
          	<a class="dropdown-item" href="<?= site_url('Home') ?>">Preview</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
</div>
<div class="container">
	<div class="toolbar mb-3 mt-3">
		<a href="<?= site_url('posts/new') ?>" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add new</a><br>
	</div>
	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-publish" role="tab" aria-controls="nav-publish" aria-selected="true">Publish  &nbsp;<span class="badge badge-pill badge-danger">	<?= $count_publish ?></span></a>
			<a class="nav-item nav-link" id="nav-draft-tab" data-toggle="tab" href="#nav-draft" role="tab" aria-controls="nav-draft" aria-selected="false">Drafts &nbsp;<span class="badge badge-pill badge-danger">	<?= $count_draft ?></span></a>
			<a class="nav-item nav-link" id="nav-trash-tab" data-toggle="tab" href="#nav-trash" role="tab" aria-controls="nav-trash" aria-selected="false">Trashed &nbsp;<span class="badge badge-pill badge-danger">	<?= $count_trash ?></span></a>
		</div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane active" id="nav-publish" role="tabpanel" aria-labelledby="nav-publish-tab">
			<br/>
			<br/>	
			<table class="table-publish">
				<thead>
					<tr>
						<th>Title</th>
						<th>Category</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php  foreach($publish_articles as $article_p): ?>
					<tr>
						<td>
							<?= $article_p->title ?>
						</td>
							<td>	<?= $article_p->category ?></td>
						<td>
							<a href="<?= site_url('posts/edit/'.$article_p->id) ?>" class="btn btn-sm btn-warning" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							<a href="#" data-delete-url="<?= site_url('posts/delete/'.$article_p->id) ?>" class="btn btn-sm btn-danger" role="button"		onclick="deleteConfirm(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>

		<div class="tab-pane fade" id="nav-draft" role="tabpanel" aria-labelledby="nav-draft-tab">
			<br/>
			<br/>
			<table class="table-draft">
				<thead>
					<tr>
						<th>Title</th>
						<th style="width: 15%;" class="text-center">Category</th>
						<th style="width: 25%;" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($draft_articles as $article): ?>
					<tr>
						<td>
							<?= $article->title ?>
						</td>
							<td>	<?= $article->category ?></td>
						<td>
							<a href="<?= site_url('posts/edit/'.$article->id) ?>" class="btn btn-sm btn-warning" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							<a href="#" data-delete-url="<?= site_url('posts/delete/'.$article->id) ?>" class="btn btn-sm btn-danger" role="button"		onclick="deleteConfirm(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>

		</div>
		<div class="tab-pane fade" id="nav-trash" role="tabpanel" aria-labelledby="nav-trash-tab">
			<br/>
			<br/>
			<table class="table-trashed">
				<thead>
					<tr>
						<th>Title</th>
						<th style="width: 15%;" class="text-center">Category</th>
						<th style="width: 25%;" class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($trash_articles as $article): ?>
						<tr>
							<td>
								<?= $article->title ?>
							</td>
								<td>	<?= $article->category ?></td>
							<td>
								<a href="<?= site_url('posts/edit/'.$article->id) ?>" class="btn btn-sm btn-warning" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
								<!-- <a href="#" data-delete-url="<?= site_url('posts/delete/'.$article->id) ?>" class="btn btn-sm btn-danger" role="button"		onclick="deleteConfirm(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> -->
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script>
		$(document).ready( function () {
			$('.table-publish').DataTable();
			$('.table-trashed').DataTable();
			$('.table-draft').DataTable();
		} );
		function deleteConfirm(event){
			Swal.fire({
				title: 'Delete Confirmation!',
				text: 'Are you sure to delete the item?',
				icon: 'warning',
				showCancelButton: true,
				cancelButtonText: 'No',
				confirmButtonText: 'Yes Delete',
				confirmButtonColor: 'red'
			}).then(dialog => {
				if(dialog.isConfirmed){
					window.location.assign(event.dataset.deleteUrl);
				}
			});
		}
	</script>

</body>

</html>