<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php require_once('header.php'); ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <h1 class="page-header">
                    My First Blog With Codeigiter
                    <small>by DokterWeb</small>
                </h1>
					<?php if(!empty($record)):?>
						<?php foreach($record as $row): ?>
							<!-- First Blog Post -->
							<h2><?php echo $row['title'];?></h2>
							<p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo tgl_indo($row['created_date']);?></p>
							<hr>
							<hr>
							<p><?php echo $row['content'];?></p>
							<!--<a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>-->
							<hr>

							<hr>
						<?php endforeach;?>
					<?php endif;?>					
						
            </div>
				
			<!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                            <?php  foreach($category as $cat){  ?>
                                <li><a href="#"><?= $cat->category ?></a>
                            <?php  }  ?>
                            </ul>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>
<?php require_once('footer.php'); ?>
