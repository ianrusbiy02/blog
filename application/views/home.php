<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php require_once('header.php'); ?>
    <!-- Page Content -->
    <div class="container mt-5">
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <h1 class="page-header">
                    My First Blog With Codeigiter
                    <small>by DokterWeb</small>
                </h1>
								<?php if(!empty($record)):?>
									<?php foreach($record as $row): ?>
										<!-- First Blog Post -->
										<h2>
											<a href="<?php echo base_url()?>Home/readArticle/<?php echo $row['id']?>"><?php echo $row['title'];?></a>
										</h2>
										
										<p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo tgl_indo($row['created_date']);?></p>
										<hr>
										<hr>
										<p><?php echo word_limiter($row['content'],50);?></p>
										<!--<a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>-->
										<a class="btn btn-primary" href="<?php echo base_url()?>Home/readArticle/<?php echo $row['id']?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
										<hr>
									<?php endforeach;?>
								<?php endif;?>					


                <!-- Pager -->
                <ul class="pager">
                    <?php echo $pagination ;?>
                </ul>
				
            </div>

<?php require_once('sidebar.php'); ?>			
<?php require_once('footer.php'); ?>
