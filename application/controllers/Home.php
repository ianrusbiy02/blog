<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->helper(['tglindo_helper','text']);
		$this->load->model('M_Post');	
	}

	
	
	public function index()
	{
		$this->load->library('pagination');

		$config['base_url'] = base_url('home/index/');
		$config['total_rows'] = $this->M_Post->total_rows('posts');
		$config['per_page'] = 3;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close']= '</li>';
		$config['prev_link'] 	= '&lt;';
		$config['prev_tag_open']='<li>';
		$config['prev_tag_close']='</li>';
		$config['next_link'] 	= '&gt;';
		$config['next_tag_open']='<li>';
		$config['next_tag_close']='</li>';
		$config['cur_tag_open']='<li class="active disabled"><a href="#">';
		$config['cur_tag_close']='</a></li>';
		$config['first_tag_open']='<li>';
		$config['first_tag_close']='</li>';
		$config['last_tag_open']='<li>';
		$config['last_tag_close']='</li>';
		
		$this->pagination->initialize($config);		

		$limit = $config['per_page'];
		$offset = (int) $this->uri->segment(3);

		$data = array(
			'category' => $this->M_Post->getcategory('posts'),
			'record' => $this->M_Post->read('posts', $limit, $offset),
			'pagination' => $this->pagination->create_links()
		);
		$this->load->view('home',$data);
	}

	public function readArticle($id){
		$data['record']=$this->M_Post->readArticle($id);
		$this->load->view('v_single',$data);
	}


}