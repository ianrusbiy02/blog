<?php

class Posts extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_article');
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
	}

	public function index()
	{
		$data['articles'] = $this->M_article->getAllArticle();
		$data['publish_articles'] = $this->M_article->getAllPublishedArticle('Publish');
		$data['draft_articles'] = $this->M_article->getAllPublishedArticle('Draft');
		$data['trash_articles'] = $this->M_article->getAllPublishedArticle('Trash');
        $data['count_publish'] = count($data['publish_articles']);
        $data['count_draft'] = count($data['draft_articles']);
        $data['count_trash'] = count($data['trash_articles']);
        $this->load->view('admin/Posts.php', $data);
	}

	public function new()
	{
		if ($this->input->method() === 'post') {
            $this->form_validation->set_rules('title', 'Title', array('required', 'min_length[20]'));
            $this->form_validation->set_rules('category', 'Category', array('required', 'min_length[3]'));
            $this->form_validation->set_rules('content', 'Content', array('required', 'min_length[200]'));
            if($this->input->post('draft') == true){
                $status = 'Draft';
            }else{
                $status = 'Publish';
            }
			if($this->form_validation->run() === false) {
				$this->session->set_flashdata('message', 'Please insert field');
			}else{
				$article = [
					'title' => $this->input->post('title'),
					'category' => $this->input->post('category'),
					'content' => $this->input->post('content'),
					'created_date' => date('Y-m-d H:i:s'),
					'status' =>  $status
				];
				$result = $this->M_article->insertData($article);
	
				if ($result) {
					$this->session->set_flashdata('message', 'Article was created');
					return redirect('posts');
				}
			}
		}

		$this->load->view('admin/Add_post.php');
	}

	public function edit($id = null)
	{
		$data['article'] = $this->M_article->find($id);

		if (!$data['article'] || !$id) {
			show_404();
		}

		if ($this->input->method() === 'post') {
			// TODO: lakukan validasi data seblum simpan ke model
			$this->form_validation->set_rules('title', 'Title', array('required', 'min_length[20]'));
            $this->form_validation->set_rules('category', 'Category', array('required', 'min_length[3]'));
            $this->form_validation->set_rules('content', 'Content', array('required', 'min_length[200]'));
            if($this->input->post('draft') == true){
                $status = 'Draft';
            }else{
                $status = 'Publish';
            }
			$article = [
				'id' => $id,
				'title' => $this->input->post('title'),
				'category' => $this->input->post('category'),
				'content' => $this->input->post('content'),
				'updated_date' => date('Y-m-d H:i:s'),
				'status' =>  $status
			];
			
			$updated = $this->M_article->updateArticle($article);
			if ($updated) {
				$this->session->set_flashdata('message', 'Article was updated');
				redirect('posts');
			}
		}

		$this->load->view('admin/Edit_form.php', $data);
	}

	public function delete($id = null)
	{
		if (!$id) {
			show_404();
		}
		$deleted = $this->M_article->deleteArticle($id);
		if ($deleted) {
			$this->session->set_flashdata('message', 'Article was deleted');
			redirect('posts');
		}
	}
}