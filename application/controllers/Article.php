<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Article extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->library('form_validation');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0, $limit=null, $offset=null)
	{
        if(!empty($id)){
            $this->db->select('title, content, category, status');
            $data = $this->db->get_where("posts", ['id' => $id])->row_array();
        }else if($limit!=null && $offset != null){
            $this->db->select('title, content, category, status');
            $this->db->limit($limit,  $offset);
            $data = $this->db->get("posts")->result();
        }else{
            $this->db->select('title, content, category, status');
            $data = $this->db->get("posts")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {     
        $this->form_validation->set_rules('title', 'Title', array('required', 'min_length[20]'));
        $this->form_validation->set_rules('category', 'Category', array('required', 'min_length[3]'));
        $this->form_validation->set_rules('content', 'Content', array('required', 'min_length[200]'));
        if($this->form_validation->run() === false) {
            $this->response([
                'status' => False,
                'message' => 'Please List The Field'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $input = $this->input->post();
            $this->db->insert('posts',$input);
         
            $this->response(['Article created successfully.'], REST_Controller::HTTP_OK);
        }
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $this->form_validation->set_rules('title', 'Title', array('required', 'min_length[20]'));
        $this->form_validation->set_rules('category', 'Category', array('required', 'min_length[3]'));
        $this->form_validation->set_rules('content', 'Content', array('required', 'min_length[200]'));
        if($this->form_validation->run() === false) {
            $this->response([
                'status' => False,
                'message' => 'Please List The Field'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $input = $this->put();
            $this->db->update('posts', $input, array('id'=>$id));
            $this->response(['Article updated successfully.'], REST_Controller::HTTP_OK);
        }

      
     
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('posts', array('id'=>$id));
        $this->response(['Article deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}